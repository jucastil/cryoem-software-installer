#!/bin/bash
#########################
##      software check GUI
##      Juan.Castillo@biophys.mpg.de
##      Last update 17.06.24

# Source installers and removers
source $PWD/installers.sh
source $PWD/removers.sh

# Check if --txt option is provided
TXT_MODE=0
if [[ "$1" == "--txt" ]]; then
    TXT_MODE=1
elif [[ "$1" != "" ]]; then
    echo "Wrong option: $1"
    echo "Try again with '--txt' or no option."
    exit 1
fi

# Check if zenity is available
ZENITY_AVAILABLE=0
if ! command -v zenity &> /dev/null; then
        echo "  Zenity not detected"
    ZENITY_AVAILABLE=1
fi

# Config file must exist
CONFIG="$PWD/config.csv"

if [ -f "$CONFIG" ]; then
    echo "config.csv exists."
else
    echo "config.csv does not exist."
    if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
        if zenity --question --title="CONFIG file not found" --text="Do you want to select the config.csv file?"; then
            CONFIG=$(zenity --file-selection --title="Select the config.csv file")
            if [ "$?" -eq 0 ]; then
                echo "You selected: $CONFIG"
            else
                echo "No file selected."
            fi
        else
            echo "User chose not to select a file."
        fi
    else
        read -p "CONFIG file not found. Do you want to select the config.csv file? (y/n): " choice
        if [ "$choice" = "y" ]; then
            read -p "Enter the path to the config.csv file: " CONFIG
            if [ -f "$CONFIG" ]; then
                echo "You selected: $CONFIG"
            else
                echo "No file selected."
            fi
        else
            echo "User chose not to select a file."
        fi
    fi
fi

# Function to edit config.csv
edit_config() {
        
    local zenity_available=$1
    local txt_mode=$2
    local config=$3
    cp "$CONFIG" "$CONFIG-"`date +%Y-%m-%d-%H-%M`    
    if [ $zenity_available -eq 0 ] && [ $txt_mode -eq 0 ]; then
        action=$(zenity --list \
            --title="Edit config.csv" \
            --text="Choose an action:" \
            --column="Action" \
            "Add Entry" \
            "Modify Entry" \
            "Cancel")

        case "$action" in
            "Add Entry")
                new_entry=$(zenity --forms --title="Add Entry" \
                    --text="Enter new key and value" \
                    --add-entry="Key" \
                    --add-entry="Value")
                if [ "$?" -eq 0 ]; then
                    echo "$new_entry" >> "$CONFIG"
                    zenity --info --text="Entry added successfully."
                fi
                ;;
            "Modify Entry")
                key=$(zenity --entry --title="Modify Entry" --text="Enter the key to modify:")
                if grep -q "^$key," "$CONFIG"; then
                    new_value=$(zenity --entry --title="Modify Entry" --text="Enter the new value for $key:")
                    sed -i "s!$key,.*!$key,$new_value!" "$CONFIG"
                    $sed -i "s/^$key,.*/$key,$new_value/" "$CONFIG"
                    zenity --info --text="Entry modified successfully."
                else
                    zenity --error --text="Key not found."
                fi
                ;;
            "Cancel")
                ;;
        esac
    else
        echo "Edit config.csv:"
        echo "1) Add Entry"
        echo "2) Modify Entry"
        echo "3) Cancel"
        read -p "Choose an action: " action

        case "$action" in
            1)
                read -p "Enter new key: " key
                read -p "Enter new value: " value
                echo "$key,$value" >> "$CONFIG"
                echo "Entry added successfully."
                ;;
            2)
                read -p "Enter the key to modify: " key
                if grep -q "^$key," "$CONFIG"; then
                    read -p "Enter the new value for $key: " new_value
                    sed -i "s!$key,.*!$key,$new_value!" "$CONFIG"
                    #sed -i "s/^$key,.*/$key,$new_value/" "$CONFIG"
                    echo "Entry modified successfully."
                else
                    echo "Key not found."
                fi
                ;;
            3)
                ;;
            *)
                echo "Invalid action."
                ;;
        esac
    fi
}



# Fill the program path array
declare -A program_paths
while IFS=',' read -r key value
do
    program_paths["$key"]="$value"
done < "$CONFIG"

# Fill resultados
resultados=()

REPORT=$PWD/programs.txt
if [ -f "$REPORT" ]; then
    rm "$REPORT"
    touch "$REPORT"
fi

printf "======================================\n  %s :  %s \n======================================\n" "$(date)" "$(hostname)" >> "$REPORT"

for program in "${!program_paths[@]}"; do
    if [ -d "${program_paths[$program]}" ]; then
        printf "| %-22s | %-15s\n" "$program" "Installed" >> "$REPORT"
        resultados+=("$program" "Installed")
    else
        if command -v "$program" &> /dev/null; then
            resultados+=("$program" "Installed")
            printf "| %-22s | %-15s\n" "$program" "Installed" >> "$REPORT"
        else
            resultados+=("$program" "missing")
            printf "| %-22s | %-15s\n" "$program" "missing" >> "$REPORT"
        fi
    fi
done

resultados_string=$(printf "%s " "${resultados[@]}")

# Show a list dialog with options
if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
    choice=$(zenity --list \
      --width=500 --height=500 \
      --title="Software check" \
      --text="Select an option:" \
      --column="Option" \
      "1) Check software list" \
      "2) Send software list report" \
      "3) Install software from list" \
      "4) Remove software from list" \
      "5) Report an issue" \
      "6) Edit config.csv" \
      "Exit")
else
    echo "Select an option:"
    echo "1) Check software list"
    echo "2) Send software list report"
    echo "3) Install software from list"
    echo "4) Remove software from list"
    echo "5) Report an issue"
    echo "6) Edit config.csv"
    echo "Exit"
    read -p "Enter choice: " choice
fi

# Handle the user's choice
case "$choice" in
        "1"|"1) Check software list")
        #echo "$resultados_string"
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
            zenity --width=500 --height=700 --list --title="Program Status table" --text="Program Status table" \
            --column="Program" --column="Status" \
            $resultados_string
        else
            printf "%-22s %-15s\n" "Program" "Status"
            printf "================================\n"
            printf "%-22s %-15s\n" "${resultados[@]}"
        fi
        ;;
        "2"|"2) Send software list report")
        email_address="jucastil@biophys.mpg.de" 
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
            zenity --question --text="Click YES to send email report"
            if [ $? -eq 0 ]; then
                mail -s "SW report " "$email_address" < "$REPORT"
                zenity --info --text="Report sent. \nPlease wait for feedback."
            else
                zenity --info --text="Report not sent"
            fi
        else
            read -p "Click YES to send email report (y/n): " choice
            if [ "$choice" = "y" ]; then
                mail -s "SW report " "$email_address" < "$REPORT"
                echo "Report sent. Please wait for feedback."
            else
                echo "Report not sent."
            fi
        fi
        ;;
        "3"|"3) Install software from list")
                options=""
                for ((i=0; i<${#resultados[@]}; i+=2)); do
                        options+="FALSE ${resultados[i]} ${resultados[i+1]} "
                done
                if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                        program=$(zenity --width=500 --height=700 --list --radiolist --text 'Select software to install\nOne per run, please!' \
            --column 'Select' --column 'Software name' --column 'Status' \
            $options)
                else
                        echo "Select software to install (one per run):"
                        select program in "${resultados[@]}"; do
                                break
                        done
                fi
                if [ -z "$program" ]; then
                        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                                zenity --error --title="Error" --text="No software selected."
                        else
                                echo "No software selected."
                        fi
                else
                        #echo "Trying to install $program"
                        case $program in
                                "topaz-0.25")
                                        # topaz doesn't need external SW
                                        echo "Trying to install $program"
                                        install_topaz $ZENITY_AVAILABLE $TXT_MODE 
                                ;; 
                                "cryolo-1.9.9")
                                        # cryolo doesn't need external SW
                                        echo "Trying to install" $program
                                        install_cryolo $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "pyem-0.5")
                                        # pyem git clones the external repo
                                        echo "Trying to install" $program
                                        install_pyem $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "chimerax")
                                        # chimerax needs a "software" rpm file
                                        echo "Trying to install" $program
                                        install_chimerax $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "chimera-1.17.3")
                                        # chimera needs a "software bin" file 
                                        echo "Trying to install" $int
                                        install_chimera $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                
                                "Gctf-v1.06")
                                        # Gctf is a binary bundle
                                        echo "Trying to install" $int
                                        install_gctf $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "eman2-sphire-sparx")
                                        # eman2 is a installer
                                        echo "Trying to install" $program
                                        install_eman2 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "conda")
                                        # miniconda is an installer
                                        echo "Trying to install" $program
                                        install_conda $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "imod-4.11.25")
                                        #IMOD installer
                                        echo "Trying to install" $program
                                        install_imod4 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "ctffind-4.1.13")
                                        #ctffind 4 raw code
                                        echo "Trying to install" $program
                                        install_ctffind4 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "coot-0.9.7")
                                        # wget coot, coot too big (4.8 GB) 
                                        echo "  Trying to install" $program
                                        install_coot $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "relion-4.0.1-stable")
                                        # git clone relion-4.0.1-stable and compile
                                        echo "  Trying to install" $program
                                        install_relion4 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "relion-5.0-beta2")
                                        # git clone relion-5.0-beta2 and compile
                                        echo "  Trying to install" $program
                                        install_relion5 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "openmpi-5.0.3")
                                        # wget openmpi-5.0.3 and compile
                                        echo "  Trying to install" $program
                                        install_openmpi5 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "gnuplot")
                                        # from yum, in principle no issues
                                        echo "  Trying to install" $program
                                        install_gnuplot $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "MotionCorr2")
                                        # MotionCorr2 from software, precompiled
                                        echo "  Trying to install" $program
                                        install_motioncorr2 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "python")
                                        echo "Trying to install" $program
                                        echo "  *NOTE* : python install done with conda on : " $PWD
                                        install_conda $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "cuda-8.0")
                                        # cuda too big (3.3 GB) so not in the software folder 
                                        echo "Trying to install" $program
                                        install_cuda $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "phenix 1.21")
                                        # phenix too big (19G) so not in the software folder
                                        echo "Trying to install" $program
                                        install_phenix $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "ccp4-8.0")
                                        # ccp4-8.0 too big (2.5 GB) so not in the software folder
                                        echo "  Trying to install" $program
                                        install_ccp4 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                    * )
                                        echo "  Invalid option"
                                        exit 1
                                ;;       
                        esac
                fi
            ;;
            
                "4"|"4) Remove software from list")
                options=""
                for ((i=0; i<${#resultados[@]}; i+=2)); do
                        options+="FALSE ${resultados[i]} ${resultados[i+1]} "
                done
                if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                        program=$(zenity --width=500 --height=700 --list --radiolist --text 'Select software to remove\nOne per run, please!' \
            --column 'Select' --column 'Software name' --column 'Status' \
            $options)
                else
                        echo "Select software to remove (one per run):"
                        select program in "${resultados[@]}"; do
                                break
                        done
                fi
                if [ -z "$program" ]; then
                        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                                zenity --error --title="Error" --text="No software selected."
                        else
                                echo "No software selected."
                        fi
                else
                        #echo "Trying to remove $program"
                        case $program in
                                # Your software installation cases here
                                "topaz-0.25")
                                        echo "Trying to remove $program"
                                        remove_topaz $ZENITY_AVAILABLE $TXT_MODE
                                ;; 
                                "cryolo-1.9.9")
                                        # cryolo doesn't need external SW
                                        echo "Trying to remove " $program
                                        remove_cryolo $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "pyem-0.5")
                                        # pyem git clones the external repo
                                        echo "Trying to remove " $program
                                        remove_pyem $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "chimerax")
                                        # chimerax needs a "software" rpm file
                                        echo "Trying to remove" $program
                                        remove_chimerax $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "chimera-1.17.3")
                                        # chimera needs a "software bin" file 
                                        echo "Trying to remove" $int
                                        remove_chimera $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "conda")
                                        echo "Trying to remove" $program
                                        remove_conda $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "Gctf-v1.06")
                                        # Gctf is a binary bundle
                                        echo "Trying to remove" $int
                                        remove_gctf $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "eman2-sphire-sparx")
                                        # eman2 is a removeer
                                        echo "Trying to remove" $program
                                        remove_eman2 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "conda")
                                        # miniconda is an removeer
                                        echo "Trying to remove" $program
                                        remove_conda $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "imod-4.11.25")
                                        #IMOD removeer
                                        echo "Trying to remove" $program
                                        remove_imod4 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "ctffind-4.1.13")
                                        #ctffind 4 raw code
                                        echo "Trying to remove" $program
                                        remove_ctffind4 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "coot-0.9.7")
                                        # wget coot, coot too big (4.8 GB) 
                                        echo "  Trying to remove" $program
                                        remove_coot $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "relion-4.0.1-stable")
                                        # git clone relion-4.0.1-stable and compile
                                        echo "  Trying to remove" $program
                                        remove_relion4 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "relion-5.0-beta2")
                                        # git clone relion-5.0-beta2 and compile
                                        echo "  Trying to remove" $program
                                        remove_relion5 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "openmpi-5.0.3")
                                        # wget openmpi-5.0.3 and compile
                                        echo "  Trying to remove" $program
                                        remove_openmpi5 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "gnuplot")
                                        # from yum, in principle no issues
                                        echo "  Trying to remove" $program
                                        yum remove -y gnuplot
                                ;;
                                "MotionCorr2")
                                        # MotionCorr2 from software, precompiled
                                        echo "  Trying to remove" $program
                                        remove_motioncorr2 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "python")
                                        echo "Trying to remove" $program
                                        echo "  *NOTE* : python remove done with conda on : " $PWD
                                        remove_conda $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "cuda-8.0")
                                        # cuda too big (3.3 GB) so not in the software folder 
                                        echo "Trying to remove" $program
                                        remove_cuda $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "phenix 1.21")
                                        # phenix too big (19G) so not in the software folder
                                        echo "Trying to remove" $program
                                        remove_phenix $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                                "ccp4-8.0")
                                        # ccp4-8.0 too big (2.5 GB) so not in the software folder
                                        echo "  Trying to remove" $program
                                        remove_ccp4 $ZENITY_AVAILABLE $TXT_MODE
                                ;;
                    * )
                                        echo "  Invalid option"
                                        exit 1
                                ;;       
                                
                        esac
                fi
                ;;
         "5"|"5) Report an issue")
                ISSUE=$PWD/issues.txt
                if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                    result=$(zenity --width=500 --height=300 --forms --title="Software Report" \
                   --text="Software Report form\n\nPlease enter the details below\n\nClick OK to submit" \
                   --add-entry="First Name" \
                   --add-entry="Last Name"  \
                   --add-entry="Program" \
                   --add-entry="Error (be brief)"  )
                    if [ "$?" -eq 0 ]; then
                        printf  "==============\n  `date` :  `hostname` \n=============\n"  >> $ISSUE
                        firstName=$(echo $result | cut -d'|' -f1)
                        lastName=$(echo $result | cut -d'|' -f2)
                        ErrorProgram=$(echo $result | cut -d'|' -f3)
                        ErrorMsg=$(echo $result | cut -d'|' -f4)
                        echo "First Name: $firstName" >> $ISSUE
                        echo "Last Name: $lastName"  >> $ISSUE
                        echo "Failing Program: $ErrorProgram">> $ISSUE
                        echo "Error Message: $ErrorMsg"  >> $ISSUE
                        email_address="jucastil@biophys.mpg.de"
                        mail -s "Error report " $email_address < $ISSUE
                        zenity --info --text="Error report submitted.\nPlease wait for feedback from the admins.\nThank you!" 
                    else
                        echo "No information entered."
                    fi
                else
                    echo "Software Report form"
                    read -p "First Name: " firstName
                    read -p "Last Name: " lastName
                    read -p "Program: " ErrorProgram
                    read -p "Error (be brief): " ErrorMsg
                    printf  "==============\n  `date` :  `hostname` \n=============\n"  >> $ISSUE
                    echo "First Name: $firstName" >> $ISSUE
                    echo "Last Name: $lastName"  >> $ISSUE
                    echo "Failing Program: $ErrorProgram">> $ISSUE
                    echo "Error Message: $ErrorMsg"  >> $ISSUE
                    email_address="jucastil@biophys.mpg.de"
                    mail -s "Error report " $email_address < $ISSUE
                    echo "Error report submitted. Please wait for feedback from the admins. Thank you!"
                fi
        ;;
        "6"|"6) Edit config.csv")
                edit_config $ZENITY_AVAILABLE $TXT_MODE $CONFIG 
        ;;
        "E"|"Exit")
                if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                    zenity --info --title="Exit" --text="Exiting the program."
                else
                    echo "Exiting the program."
                fi
        ;;
    *)
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
            zenity --error --title="Error" --text="Invalid option."
        else
            echo "Invalid option."
        fi
    ;;
esac