# removers for the different programs

popup_done(){

	ZENITY_AVAILABLE=$1
	TXT_MODE=$2
	
	if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
	 	zenity --info --title="Done" --text="Removing of program finished"
	else
		echo "Removing done."
	fi	
	
}


remove_topaz(){
	
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2

	conda env remove -n topaz -y
	rm /usr/share/Modules/modulefiles/topaz-0.25
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}

remove_cryolo(){
	
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2

	#conda env remove -n cryolo --all
	conda env remove -n cryolo -y
	rm /usr/share/Modules/modulefiles/cryolo-1.9.9
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}

remove_pyem(){
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2

	#conda env remove -n pyem --all
	conda env remove -n pyem -y
	rm /usr/share/Modules/modulefiles/pyem-0.5
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}

remove_chimerax(){
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2

	rm -rf /usr/bin/chimerax
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}

remove_chimera(){
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2

	rm -rf /opt/local/software/chimera-1.17.3/
	rm -rf /usr/share/Modules/modulefiles/chimera-1.17.3
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}

remove_gctf(){
	
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2
	
	rm -rf /opt/local/software/Gctf_v1.06/
	rm -rf /usr/share/Modules/modulefiles/Gctf-v1.06
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}

remove_eman2(){
	
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2
	
	rm -rf /opt/local/software/eman2-sphire-sparx/
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}

remove_conda(){

	ZENITY_AVAILABLE=$1
	TXT_MODE=$2
		
	rm -rf /opt/local/software/miniconda
	popup_done $ZENITY_AVAILABLE $TXT_MODE

}

remove_ctffind4(){
	
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2

	rm -rf /opt/local/software/ctffind-4.1.13/
	rm -rf /usr/share/Modules/modulefiles/ctffind-4.1.13
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}

remove_coot(){
	
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2
	rm -rf /opt/local/software/coot-Linux-x86_64-centos-7-gtk2-python/
	rm -rf /usr/share/Modules/modulefiles/coot-0.9.7
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}

remove_relion4(){
	
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2
	
	rm -rf /opt/local/software/relion-4.0.1-stable/
	rm -rf /usr/share/Modules/modulefiles/relion-4.0.1-stable 
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}

remove_relion5(){
	
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2
	
	rm -rf /opt/local/software/relion-5.0-beta2/
	rm -rf /opt/local/software/relion-5.0-beta2_torch/
	rm -rf /usr/share/Modules/modulefiles/relion-5.0-beta2
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}

remove_openmpi5(){
	
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2
	
	rm -rf /opt/local/software/openmpi-5.0.3/
	rm -rf /usr/share/Modules/modulefiles/openmpi-5.0.3
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}

remove_motioncorr2(){
	
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2
	
	rm -rf /opt/local/repos/MotionCor2/
	rm -rf /usr/share/Modules/modulefiles/MotionCor2
	popup_done $ZENITY_AVAILABLE $TXT_MODE
}

remove_cuda(){
	
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2
	
	rm -rf /opt/local/software/cuda/
	rm -rf /usr/share/Modules/modulefiles/cuda-8.0
	popup_done $ZENITY_AVAILABLE $TXT_MODE
	
}


remove_phenix(){

	ZENITY_AVAILABLE=$1
	TXT_MODE=$2
	
	rm -rf /opt/local/software/phenix/
	rm -rf /usr/share/Modules/modulefiles/phenix-1.21.1
	popup_done $ZENITY_AVAILABLE $TXT_MODE
}

remove_ccp4(){
	
	ZENITY_AVAILABLE=$1
	TXT_MODE=$2
	
	rm -rf /opt/local/software/ccp4-8.0/ccp4-8.0
	rm -rf /usr/share/Modules/modulefiles/
	popup_done $ZENITY_AVAILABLE $TXT_MODE
}
