#!/bin/bash

# Define the paths
declare -A program_paths
program_paths=(
    ["ccp4-8.0"]="/opt/local/software/ccp4-8.0/ccp4-8.0/bin/"
    ["conda"]="/opt/local/software/miniconda/bin/"
    ["coot-0.9.7"]="/opt/local/software/coot-Linux-x86_64-centos-7-gtk2-python/bin"
    ["cuda-8.0"]="/opt/local/software/cuda/8.0.44/"
    ["chimerax"]="/usr/bin/chimerax"
    ["chimera 1.17.3"]="/opt/local/software/chimera-1.17.3/"
    ["cryolo 1.9.9"]="/opt/local/software/miniconda/envs/cryolo/"
    ["ctffind"]="/usr/local/bin/ctffind"
    ["Gctf-v1.06"]="/opt/local/software/Gctf_v1.06/bin/"
    ["imod"]="/path/to/imod"
    ["eman2.91"]="/opt/local/software/eman2/"
    ["mpirun"]="/opt/openmpi-3.1.6/bin/"
    ["phenix"]="/soft/software/phenix/phenix-1.21.1-5286/build/bin/"
    ["python"]="/opt/local/software/miniconda/bin/"
    ["pyem"]="/opt/local/software/miniconda/envs/pyem"
    ["relion-4.0.1-stable"]="/opt/local/repos/relion/build/bin/"
    ["relion-5.0-beta2"]="/opt/local/repos/relion_5/build/bin/"
    ["MotionCorr2"]="/path/to/MotionCorr2"
    ["ctffind"]="/path/to/ctffind"
    ["topaz 0.25"]="/opt/local/software/miniconda/envs/topaz/bin/"
    ["gnuplot"]="/path/to/gnuplot"
)

# Color codes
RED='\033[0;31m'
NC='\033[0m' # No Color

# Header
printf "| %-22s | %-15s\n" "Program" "Status"
printf "| %-22s | %-15s\n" "---------------------" "--------------------"

# Check if the program folder is there, if not, print a message
for program in "${!program_paths[@]}"; do
    if [ -d "${program_paths[$program]}" ]; then
        printf "| %-22s | %-15s\n" "$program" "Installed"
        #echo "	$program seems to be installed"
    else
        #check if the program is installed (as a command)
        if command -v $program &> /dev/null; then
                printf "| %-22s | %-15s\n" "$program" "Installed (command)"
		else
				printf "| ${RED} %-20s ${NC} | ${RED}  %-15s${NC}\n" "$program" "missing / not found"
		fi
    fi
done

