# installers for the different programs


install_topaz(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        if command -v conda &> /dev/null; then
                conda create -n topaz python=3.11 -y && conda install -n topaz topaz -c tbepler -c pytorch -y && conda install -n topaz cudatoolkit=9.0 -c pytorch -y
                echo "  Copying module definition"
                cp modules/topaz-0.25 /usr/share/Modules/modulefiles/
                if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                        zenity --info --title="Done" --text="Installation of $program finished"
                else
                        echo "Installation done."
                fi
        else
                if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                        zenity --error --title="Error" --text="No conda available, please install it first"
                else
                        echo " No conda available, please install it first"
                fi
        fi
}


install_cryolo(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        if command -v conda &> /dev/null; then
                conda create -y -n cryolo -c conda-forge -c anaconda pyqt=5 python=3 numpy=1.18.5 libtiff wxPython=4.1.1 adwaita-icon-theme 'setuptools<66' && conda activate cryolo && pip install -y nvidia-pyindex && pip install -y 'cryolo[c11]'
                echo "  Copying module definition"
                cp modules/cryolo-1.9.9 /usr/share/Modules/modulefiles/
                if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                        zenity --info --title="Done" --text="Installation of $program finished"
                else
                        echo "Installation done."
                fi
        else
                if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                        zenity --error --title="Error" --text="No conda available, please install it first"
                else
                        echo " No conda available, please install it first"
                fi
        fi

        }

install_pyem(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        if command -v conda &> /dev/null; then
                conda create -n pyem python=3.11 --yes && conda activate pyem && conda install numpy scipy matplotlib seaborn numba pandas natsort --yes 
                #conda create -n pyem --yes && conda activate pyem && conda install numpy scipy matplotlib seaborn numba pandas natsort --yes 
                conda install -c conda-forge pyfftw healpy pathos --yes && git clone https://github.com/asarnow/pyem.git && cd pyem && pip install --no-dependencies -e .
                rm -rf pyem/
                echo "  Copying module definition"
                cp modules/pyem-0.5 /usr/share/Modules/modulefiles/
                if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                        zenity --info --title="Done" --text="Installation of $program finished"
                else
                        echo "Installation done."
                fi
        else
                if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                        zenity --error --title="Error" --text="No conda available, please install it first"
                else
                        echo " No conda available, please install it first"
                fi
        fi
        
}

install_chimera(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        cp software/chimera-1.17.3-linux_x86_64.bin .
        echo "  Copying module definition"
        cp modules/chimera-1.17.3 /usr/share/Modules/modulefiles/
        mkdir -p /opt/local/software/chimera-1.17.3
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        tput setaf 3;
        echo
        echo "  Sorry no easy way to do this"
        echo "  Please now go to shell and run the binary chimera-1.17.3-linux_x86_64.bin"
        echo
        tput sgr 0;
        
}

install_chimerax(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        cp software/ucsf-chimerax-1.7.1-1.el8.x86_64.rpm .
        yum -y install mesa-libOSMesa-23.1.4-1.el9.x86_64.rpm ucsf-chimerax-1.7.1-1.el8.x86_64.rpm
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        
}

install_gctf(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        echo "  Copying sw to /opt/local/software/" 
        cp software/Gctf_v1.06_and_examples.tar.gz /opt/local/software/
        echo "  Copying module definition"
        cp modules/Gctf-v1.06 /usr/share/Modules/modulefiles/
        cd /opt/local/software/ && tar -xvzf Gctf_v1.06_and_examples.tar.gz
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        
}

install_eman2(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        echo "  Copying sw" 
        cp software/eman2.91_sphire1.4_sparx.linux64.sh .
        tput setaf 3;
        echo
        echo "  WARNING: running installer"
        echo "  This may take time! even it may look like frozen"
        echo "  ...specially while solving dependencies"
        echo
        tput sgr 0;
        bash eman2.91_sphire1.4_sparx.linux64.sh -p /opt/local/software/eman2-sphire-sparx
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        
}

install_conda(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        echo "  Bringing SW from repo to " $PWD
        curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
        echo "  running the installer "
        bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/local/software/miniconda
        rm Miniconda3-latest-Linux-x86_64.sh
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        
}

install_ctffind4(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2

        yum -y install sudo epel-release 
        yum -y install fftw fftw-devel wxGTK3 wxGTK3-devel libtiff libtiff-devel cmake make gcc git which diffutils gcc-c++ libjpeg-turbo-devel
        #cannot wget the file: we get it from the repository
        #cd /opt/local/software/ && wget http://grigoriefflab.janelia.org/sites/default/files/ctffind-4.1.13.tar.gz 
        echo "  Copying SW to final location"
        cp software/ctffind-4.1.13.tar /opt/local/software/
        echo "  Copying module definition"
        cp modules/ctffind-4.1.13 /usr/share/Modules/modulefiles/
        echo "  Configuring and compiling"
        cd /opt/local/software/ && tar -xvf ctffind-4.1.13.tar
        cd ctffind-4.1.13/
        ./configure --disable-debugmode
        sed -i '/#include "pdb.h"/d' src/core/core_headers.h
        sed -i '/#include "water.h"/d' src/core/core_headers.h
        make
        rm ctffind-4.1.13.tar
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        
}

install_coot(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        echo "  Bringing SW from repo to " $PWD
        #cp /home/admin/INSTALL/SoftwareRepos/CentOS8-AMD/repos/coot.tar .
        wget https://www2.mrc-lmb.cam.ac.uk/personal/pemsley/coot/binaries/release/coot-0.9.7-binary-Linux-x86_64-centos-7-python-gtk2.tar.gz 
        echo "  Copying module definition"
        cp modules/coot-0.9.7 /usr/share/Modules/modulefiles/
        echo "  Installing missing libs (if any)"
        yum install libpng15 libGLU libcrypto\* -y
        echo "  Unzipping and moving to coot folder"
        tar -xvf coot-0.9.7-binary-Linux-x86_64-centos-7-python-gtk2.tar.gz
        mv coot-Linux-x86_64-centos-7-gtk2-python/ coot/ 
        echo "  Patching libs"
        cp -nf /lib64/libcrypto.so.10 coot/coot-Linux-x86_64-centos-7-gtk2-python/lib
        echo "  Moving the installation to final location"
        mv coot/coot-Linux-x86_64-centos-7-gtk2-python/ /opt/local/software/
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        
}

install_relion4(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        WD=$PWD
        
        module load openmpi-5.0.3
        echo "  Cloning repo"
        git clone https://github.com/3dem/relion.git
        echo "  Copying module definition"
        cp modules/relion-4.0.1-stable /usr/share/Modules/modulefiles/
        cd relion && git checkout ver4.0 && mkdir build
        cd build && cmake -DCUDA=ON -DCudaTexture=ON -DFORCE_OWN_FLTK=ON -DMPI_C_COMPILER=mpicc -DMPI_CXX_COMPILER=mpicxx -DCMAKE_INSTALL_PREFIX=/opt/local/software/relion-4.0.1-stable -DCMAKE_C_FLAGS=-lm -DCMAKE_CXX_FLAGS=-lm -Wno-dev ..
        make -j 8
        make install
        cd $WD
        rm -rf relion
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
}

install_relion5(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        WD=$PWD
        
        module load openmpi-5.0.3
        mkdir -p /opt/local/software/relion-5.0-beta2
        mkdir -p /opt/local/software/relion-5.0-beta2_torch
        cp modules/relion-5.0-beta2 /usr/share/Modules/modulefiles/
        git clone https://github.com/3dem/relion.git 
        tput setaf 3;
        echo 
        echo "  WARNING: the next step (conda env create) takes very long! "
        echo
        tput sgr0;
        cd relion && git checkout ver5.0 && mkdir build 
        if command -v conda &> /dev/null; then
                conda env create -f environment.yml
                conda env update -f environment.yml
        else
                if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                        zenity --error --title="Error" --text="No conda available, please install it first"
                else
                        echo " No conda available, please install it first"
                fi
                exit 1
        fi
        cd build && cmake -DCUDA=ON -DCudaTexture=ON -DFORCE_OWN_FLTK=ON -DMPI_C_COMPILER=mpicc -DMPI_CXX_COMPILER=mpicxx \
        -DCMAKE_INSTALL_PREFIX=/opt/local/software/relion-5.0-beta2 \
        -DPYTHON_EXE_PATH=/opt/local/software/miniconda/envs/relion-5.0/bin/python \
        -DTORCH_HOME_PATH=/opt/local/software/relion-5.0-beta2_torch \
        -DCMAKE_C_FLAGS=-lm -DCMAKE_CXX_FLAGS=-lm -Wno-dev ..
        make -j 8
        make install
        cd $WD
        rm -rf relion
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        
}

install_openmpi5(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        echo "  Getting SW"
        wget https://download.open-mpi.org/release/open-mpi/v5.0/openmpi-5.0.3.tar.gz
        echo "  Copying module definition"
        cp modules/openmpi-5.0.3 /usr/share/Modules/modulefiles/
        echo "  untaring and compiling"
        tar -xvf openmpi-5.0.3.tar.gz
        cd openmpi-5.0.3
        mkdir build && cd build
        ../configure --prefix=/opt/local/software/openmpi-5.0.3
        make all        
        make install
        rm -rf openmpi-5.0.3/
        rm -rf openmpi-5.0.3.tar.gz
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        
}

install_gnuplot(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        yum install gnuplot -y
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        
}

install_motioncorr2(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        cp modules/MotionCor2 /usr/share/Modules/modulefiles/
        cp software/MotionCor2_1.6.4_Mar31_2023.zip /opt/local/repos/MotionCor2/
        mkdir -p /opt/local/repos/MotionCor2/
        cd /opt/local/repos/MotionCor2/ && unzip MotionCor2_1.6.4_Mar31_2023.zip
        echo "  Done"
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        
}

install_imod4(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        cp software/imod_4.11.25_RHEL7-64_CUDA8.0.sh .
        echo "Y" | ./imod_4.11.25_RHEL7-64_CUDA8.0.sh -dir /opt/local/software
        rm imod_4.11.25_RHEL7-64_CUDA8.0.sh
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        
}

install_cuda(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        echo "  Bringing SW from repo to /opt/local/software/" 
        rsync -av /home/admin/INSTALL/SoftwareRepos/CentOS8-AMD/software/cuda /opt/local/software/
        cp modules/cuda-8.0 /usr/share/Modules/modulefiles/
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        
}

install_phenix(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        echo "  Bringing SW from repo to /opt/local/software/" 
        rsync -av /home/admin/INSTALL/SoftwareRepos/CentOS8-AMD/software/phenix /opt/local/software/
        cp modules/phenix-1.21.1 /usr/share/Modules/modulefiles/
        if [ $ZENITY_AVAILABLE -eq 0 ] && [ $TXT_MODE -eq 0 ]; then
                zenity --info --title="Done" --text="Installation of $program finished"
        else
                echo "Installation done."
        fi
        echo "Installation of phenix finished"
        
}

install_ccp4(){
        
        ZENITY_AVAILABLE=$1
        TXT_MODE=$2
        
        cp /home/admin/INSTALL/SoftwareRepos/CentOS8-AMD/repos/ccp4-8.0.019-linux64.tar.gz .
        cp modules/ccp4-8.0 /usr/share/Modules/modulefiles/
        tar vxfz ccp4-8.0.019-linux64.tar.gz
        mv ccp4-8.0/ /opt/local/software/
        echo "  Done"
                
        
        }