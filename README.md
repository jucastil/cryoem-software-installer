# cryoem software installer

Just a wrap around a lot of installers for cryo EM, with some additional tools

## Getting started

Clone this repo in your local computer.

```
git clone https://gitlab.mpcdf.mpg.de/jucastil/cryoem-software-installer.git
cd cryoem_software_installer
chmod 777 software_check_*.sh
```

You will end up with two files and one folder.

`software_check_TXT.sh` : sofware scanner. It looks for the listed programs in the default locations.

`software_check_GUI.sh` : the GUI version. It looks for the listed programs in the default locations, plus it lets you install the missing ones.

## Installation

Since this is an overgrown bash script, there is no need to do anything but to clone the repo. 

## Usage

It should be self-explanatory. Contact me if it's not the case.

## Support

If it doesn't work, let me know.

## Roadmap

- SUDO Support. At this moment only root can ask for the installations
- Add a configuration file, with the default paths to the programs and master email address
- Add a version controller, so that one can choose the sofware version to install
- Add a database call, so that one can check what happened before the current run
- Add a OK/NOK option. It's going to be difficult because some of the programs are quite complex.

## Contributing

If you want to contribute, let me know and we'll see what we can do.

## Authors and acknowledgment

Well, we'll see. 

## License

This is opensource. Of course each program from the list has a license.

## Project status

I have tested the scripts on CentOS 7.X and CentOS 8.X. 

Since it's a bash script, I guess it will work in other systems.

The plan is to add support at least for Ubuntu and openSUSE.
